# GandCrab_Research_Paper

Describes the GandCrab threat group and its development of a new malware-based franchise business model.  Includes detailed sandbox analysis of a version 3 GandCrab malware instance.